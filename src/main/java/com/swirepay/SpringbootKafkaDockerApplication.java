package com.swirepay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootKafkaDockerApplication {

	public static void main(String[] args) {
		System.out.println("hello world");
		SpringApplication.run(SpringbootKafkaDockerApplication.class, args);
	}

}
