package com.swirepay;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class Consumer {

    @KafkaListener(topics = "SP_TEST_TOPIC",groupId = "group_id")
    public void consumeMessage(String message){

        System.out.println(message);
    }

    /*@KafkaListener(topics = "SP_PAYOUT_TOPIC",groupId = "group_id")
    public void consumeMessageTest(String message){

        System.out.println(message);
    }*/
}
